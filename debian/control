Source: pyvtk
Section: python
Priority: optional
Maintainer: Steve M. Robbins <smr@debian.org>
Uploaders: Debian Python Team <team+python@tracker.debian.org>
Build-Depends: debhelper (>= 5.0.38), dh-python, python (>= 2.3.5-11)
Standards-Version: 3.9.1
XS-Python-Version: all
Homepage: http://cens.ioc.ee/projects/pyvtk/
Vcs-Git: https://salsa.debian.org/python-team/packages/pyvtk.git
Vcs-Browser: https://salsa.debian.org/python-team/packages/pyvtk

Package: python-pyvtk
Architecture: all
Depends: ${python:Depends}, ${misc:Depends}
Provides: ${python:Provides}
XB-Python-Version: ${python:Versions}
Description: module for manipulating VTK files
 PyVTK provides python classes to read and write a VTK file and
 to create a VTK file from standard Python objects.
 Only VTK File Format version 2.0 is supported.
 Features include:
   - ascii and binary output, ascii input
   - DataSet formats:
       StructuredPoints, StructuredGrid, RectilinearGrid,
       PolyData, UnstructuredGrid
   - Data formats:
       PointData, CellData
   - DataSetAttr formats:
       Scalars, ColorScalars, LookupTable, Vectors,
       Normals, TextureCoordinates, Tensors, Field
